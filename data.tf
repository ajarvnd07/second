data "archive_file" "init" {
  type        = "zip"
  source_dir  = "./code"
  output_path = "./output/code.zip"
}
